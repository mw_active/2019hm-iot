#!/bin/bash
##############################################################
# Performs all the required installation steps to bring all
# Linux machines (x86 and arm) into a state to either build
# or run tmls-ros:
# * ARM based Ubuntu distributions
#   * installs required ubuntu repositories and packages
#   * Installs ROS ${ROS_VERSION}
###############################################################

export BOLD='\e[1;31m'         # Bold Red
export REV='\e[1;32m'          # Bold Green
export OFF=$(tput sgr0)

echo -e "${REV}==========================================${OFF}"
echo -e "${REV}= Installation of all dependencies       =${OFF}"
echo -e "${REV}= * Basic system                         =${OFF}"
echo -e "${REV}= * ROS Melodic                          =${OFF}"
echo -e "${REV}==========================================${OFF}"


## Check that the script is run as root

if ! [ $(id -u) = 0 ]; then
   echo -e "${BOLD}The script need to be run as root via sudo.${OFF}"
   exit 1
fi


## Find out original User
echo -e " - Find non-root user"

if [ $SUDO_USER ]; then
    real_user=$SUDO_USER
else
    real_user=$(whoami)
fi
echo -e "    Non root user: ${REV}${real_user}${OFF}"

## Find script path
path_tmp=$(readlink -f "${BASH_SOURCE[0]}")
PATH_ACTUAL=$(dirname "$path_tmp")
ROS_VERSION="melodic"

## Parse commandline

usage()
{
    echo "usage: fiji-install required.sh [-in]"
    echo "   -i | --interactive     Enter interactive mode for package installation"
    echo "   -n | --no-install      No package installation"
    echo "   -N | --no-install-cfg  No post install steps"
}

while [ "$1" != "" ]; do
    case $1 in
        -i | --interactive )    echo -e "${REV} - Enter interactive mode for package install ${OFF}"
                                flag_interactive=1
                                ;;
        -n | --no-install )     echo -e "${REV} - No installation, only file system sync ${OFF}"
                                flag_no_install=1
                                ;;
        -N | --no-install-cfg ) echo -e "${REV} - No post-install steps ${OFF}"
                                flag_no_install_pkg=1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

##
## Packageinstallation
##

if [ -z ${flag_no_install+x} ]; then
  ## Add dependency repositories
  echo -e "${REV}==========================================${OFF}"
  echo -e "${REV}= Add repositories                       =${OFF}"
  echo -e "${REV}==========================================${OFF}"

  while true; do
    if [ -z ${flag_interactive+x} ]; then
      yn="y"
    else
      read -p "Do you want to add the required apt repositories and packages? (y|n) " yn
    fi
    case $yn in
      [Yy]* ) sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
              apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
              apt-add-repository universe
              apt-add-repository multiverse
              apt-add-repository restricted
              apt update

              echo -e "${REV}==========================================${OFF}"
              echo -e "${REV}= System Requirements                    =${OFF}"
              echo -e "${REV}==========================================${OFF}"
              apt install -y nano pps-tools chrony gpsd gpsd-clients build-essential libusb-dev
              apt install -y apt-transport-https mpg123
              apt install -y lm-sensors sysstat nmon atop htop glances
              apt install -y clusterssh pssh

              echo -e "${REV}==========================================${OFF}"
              echo -e "${REV}= Dev Tools                              =${OFF}"
              echo -e "${REV}==========================================${OFF}"
              apt install -y build-essential locate cmake-curses-gui sphinx-common
              apt install -y google-perftools libgoogle-perftools-dev
              apt install -y git git-gui
              apt install -y python-pip python-tox pandoc
              #apt install -y python3-pip

              clear
              break;;
      [Nn]* ) break;;
      * ) echo "Please answer yes or no.";;
    esac
  done



  ## PIP install packages
  while true; do
    if [ -z ${flag_interactive+x} ]; then
      yn="y"
    else
      read -p "Do you want to add the pip install required packages? (y|n) " yn
    fi
    case $yn in
      [Yy]* ) pip install tensorflow keras h5py
              pip install boto3 AWSIoTPythonSDK inject
              #pip3 install boto3 AWSIoTPythonSDK
              clear
              break;;
      [Nn]* ) break;;
      * ) echo "Please answer yes or no.";;
    esac
  done

  ## Install ROS
  echo -e "${REV}==========================================${OFF}"
  echo -e "${REV}= Install ROS                            =${OFF}"
  echo -e "${REV}==========================================${OFF}"

  while true; do
    if [ -z ${flag_interactive+x} ]; then
      yn="y"
    else
      read -p "Do you want to install ROS? (y|n) " yn
    fi
    case $yn in
      [Yy]* ) . "${PATH_ACTUAL}/../fiji-env.sh"
              ## Python Packages
              apt install -y python-rosinstall python-rosinstall-generator python-wstool python-pip python-tox pandoc
              #sudo -u $real_user pip install --upgrade pip # Potential conflict line for pip
              ## System ROS packages
              apt install -y ros-${ROS_VERSION}-navigation ros-${ROS_VERSION}-gps-common ros-${ROS_VERSION}-imu-tools ros-${ROS_VERSION}-geographic-msgs ros-${ROS_VERSION}-geodesy
              apt install -y ros-${ROS_VERSION}-rosbash
              ## For web interface and server
              apt install -y ros-${ROS_VERSION}-rosbridge-suite ros-${ROS_VERSION}-tf2-web-republisher
              ## Camera Packages:
              #  apt install -y ros-${ROS_VERSION}-cv-camera # Melodic does not have this package
              apt install -y ros-${ROS_VERSION}-usb-cam
              apt-intsall -y ros-${ROS_VERSION}-image-view
              ## OpenCV packages
              apt install -y ros-${ROS_VERSION}-vision-opencv
              ## Turtlesim
              apt install -y ros-${ROS_VERSION}-turtlesim ros-${ROS_VERSION}-turtle-tf2
              ## RQT
              apt install -y ros-${ROS_VERSION}-rqt-gui ros-${ROS_VERSION}-rqt-graph ros-${ROS_VERSION}-rqt-graph ros-${ROS_VERSION}-rqt-topic ros-${ROS_VERSION}-rqt-*
              ## RVIZ
              apt install -y ros-${ROS_VERSION}-rviz
              ## ML
              apt install -y ros-${ROS_VERSION}-tensorflow-ros ros-${ROS_VERSION}-tensorflow-ros-rqt
              apt install -y ros-${ROS_VERSION}-image-recognition ros-${ROS_VERSION}-image-recognition-*
              ## Update ROS environment
              rosdep init
              sudo -u $real_user rosdep update
              clear
              break;;
      [Nn]* ) break;;
      * ) echo "Please answer yes or no.";;
    esac
  done

  ## Raspicam node for ROS
  #while true; do
  #  if [ -z ${flag_interactive+x} ]; then
  #    yn="y"
  #  else
  #    read -p "Do you want to add the raspicam repositories (arm)? (y|n) " yn
  #  fi
  #  case $yn in
  #    [Yy]* ) sh -c 'echo "deb https://packages.ubiquityrobotics.com/ubuntu/ubiquity xenial main" > /etc/apt/sources.list.d/ubiquity-latest.list'
  #            apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key C3032ED8
  #            apt-get update
  #            apt install ros-${ROS_VERSION}-raspicam-node
  #
  #            clear
  #            break;;
  #    [Nn]* ) break;;
  #    * ) echo "Please answer yes or no.";;
  #  esac
  #done


else
  echo -e "${BOLD}Skipping package installation${OFF}"
fi

