#!/bin/bash

## Find script path
path_tmp=$(readlink -f "${BASH_SOURCE[0]}")
PATH_ACTUAL=$(dirname "$path_tmp")

python2.7 ${PATH_ACTUAL}/../thirdparty/aws-iot-device-sdk-python/samples/jobs/jobsSample.py -e a3g1ym8g0kwvjd-ats.iot.us-west-2.amazonaws.com -r ${PATH_ACTUAL}/../demo/AmazonRootCA1.pem -c ${PATH_ACTUAL}/../demo/*-certificate.pem.crt -k ${PATH_ACTUAL}/../demo/*-private.pem.key -n $(hostname)

