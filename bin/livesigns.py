#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""Standalone executable do demonstrate online status and device shadow updates

Based on the AWSIoTPythonSDK examples

* Demonstrates publishing and retrieving the device shadow
* Demonstrates MQTT topic publisher
"""

from __future__ import print_function

# AWS Imports
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTShadowClient

# Basic Library Imports
import logging
import time
import argparse
import json
import platform
import datetime
import random

AllowedActions = ['both', 'publish', 'subscribe']

#------------------------------------------------------------------------
# Function and Class Definitions
#------------------------------------------------------------------------

def customCallback(client, userdata, message):
    ''' Custom callback for topic subscribers

    Args:
        client (???): Client connection
        userdata (???): User data
        message (dict): Message containing. Contains the payload as a JSON string.

    Returns:
        none
    '''
    print("+"*30)
    print("Received a new message: ")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("+"*30 + "\n\n")

def customShadowCallback_Delta(payload, responseStatus, token):
    # payload is a JSON string ready to be parsed using json.loads(...)
    # in both Py2.x and Py3.x
    print(responseStatus)
    payloadDict = json.loads(payload)
    print("+"*30)
    print("+ Received a new shadow delta message:")
    print("property: " + str(payloadDict["state"]["property"]))
    print("version: " + str(payloadDict["version"]))
    print("+"*30 + "\n\n")

class shadowCallbackContainer:
    """
    @class <descriptive name of class>
    @brief Custom callback class for the device shadow.
    @details To be added
    """
    def __init__(self, deviceShadowInstance):
        """
        @name <descriptive name of function>
        @param deviceShadowInstance Device shadow
        @brief Container for all callbacks of the device shadow
        """
        self.deviceShadowInstance = deviceShadowInstance
        self._shadow = {}
        self._delta = {}

    def customShadowCallback_DeltaAction(self, payload, responseStatus, token):
        ''' Custom callback for the delta topic of the device shadow.


        Args:
            payload (str): the payload  is a JSON string ready to be parsed
                           using json.loads(...)
            responseStatus

        '''

        print("+"*30)
        print("+ Received a new shadow delta message:")
        payloadDict = json.loads(payload)
        deltaMessage = json.dumps(payloadDict["state"])
        print(deltaMessage)
        #print("Request to update the reported state...")
        #newPayload = '{"state":{"reported":' + deltaMessage + '}}'
        #self.deviceShadowInstance.shadowUpdate(newPayload, None, 5)
        #print("Sent.")

    def customShadowCallback_DeltaListen(self, payload, responseStatus, token):
        # payload is a JSON string ready to be parsed using json.loads(...)
        # in both Py2.x and Py3.x
        payloadDict = json.loads(payload)
        print("+"*30)
        print("Received a new shadow delta message from " + responseStatus)
        print("State:   " + str(payloadDict["state"]))
        print("version: " + str(payloadDict["version"]))
        self._delta = payloadDict
        print("+"*30 + "\n\n")

    def customShadowCallback_Update(self, payload, responseStatus, token):
        # payload is a JSON string ready to be parsed using json.loads(...)
        # in both Py2.x and Py3.x
        print("+"*30)
        print("Shadow Callback: UpdateShadow")
        if responseStatus == "timeout":
            print("Error: " + token + " time out!")
        if responseStatus == "accepted":
            payloadDict = json.loads(payload)
            print("Success: " + token + " accepted!")
            print("Payload: " + str(payloadDict))
        if responseStatus == "rejected":
            print("Error: " + token + " rejected!")
        print("+"*30 + "\n\n")

    def customShadowCallback_Get(self, payload, responseStatus, token):
        # payload is a JSON string ready to be parsed using json.loads(...)
        # in both Py2.x and Py3.x
        print("+"*30)
        print("Shadow Callback: GetShadow")
        if responseStatus == "timeout":
            print("Error: " + token + " time out!")
        elif responseStatus == "accepted":
            payloadDict = json.loads(payload)
            print("Success: " + token + " accepted!\n")
            print("Payload: " + str(payloadDict))
            self._shadow = payloadDict
        elif responseStatus == "rejected":
            print("Error: " + token + " rejected!")
        else:
            print("Error: unknown response status: " + responseStatus)

        print("+"*30+"\n\n")

#--------------------------------------------------------------------
# Read in command-line parameters
#--------------------------------------------------------------------
parser = argparse.ArgumentParser()
parser.add_argument("-e", "--endpoint", action="store", required=True, dest="host", help="Your AWS IoT custom endpoint")
parser.add_argument("-r", "--rootCA", action="store", required=True, dest="rootCAPath", help="Root CA file path")
parser.add_argument("-c", "--cert", action="store", dest="certificatePath", help="Certificate file path")
parser.add_argument("-k", "--key", action="store", dest="privateKeyPath", help="Private key file path")
parser.add_argument("-p", "--port", action="store", dest="port", type=int, help="Port number override")
parser.add_argument("-w", "--websocket", action="store_true", dest="useWebsocket", default=False,
                    help="Use MQTT over WebSocket")
parser.add_argument("-m", "--mode", action="store", dest="mode", default="both",
                    help="Operation modes: %s"%str(AllowedActions))
parser.add_argument("-M", "--message", action="store", dest="message", default="Hello World!",
                    help="Message to publish")

# Read out commandline parameters
args = parser.parse_args()
host = args.host
rootCAPath = args.rootCAPath
certificatePath = args.certificatePath
privateKeyPath = args.privateKeyPath
port = args.port
useWebsocket = args.useWebsocket

# Use the hostname as thingID and base for the clientID
thingId = platform.uname()[1]
clientIdMqtt = thingId + "-mqtt"
clientIdShadow = thingId + "-shadow"
topic = thingId + "/test"

if args.mode not in AllowedActions:
    parser.error("Unknown --mode option %s. Must be one of %s" % (args.mode, str(AllowedActions)))
    exit(2)

if args.useWebsocket and args.certificatePath and args.privateKeyPath:
    parser.error("X.509 cert authentication and WebSocket are mutual exclusive. Please pick one.")
    exit(2)

if not args.useWebsocket and (not args.certificatePath or not args.privateKeyPath):
    parser.error("Missing credentials for authentication.")
    exit(2)

# Port defaults
if args.useWebsocket and not args.port:  # When no port override for WebSocket, default to 443
    port = 443
if not args.useWebsocket and not args.port:  # When no port override for non-WebSocket, default to 8883
    port = 8883

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
#logger.setLevel(logging.DEBUG)
logger.setLevel(logging.WARNING)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

#===================================================================
# Setup to AWSIoTMQTTClient
#===================================================================

# Init AWSIoTMQTTClient
myAWSIoTMQTTClient = None
if useWebsocket:
    myAWSIoTMQTTClient = AWSIoTMQTTClient(clientIdMqtt, useWebsocket=True)
    myAWSIoTMQTTClient.configureEndpoint(host, port)
    myAWSIoTMQTTClient.configureCredentials(rootCAPath)
else:
    myAWSIoTMQTTClient = AWSIoTMQTTClient(clientIdMqtt)
    myAWSIoTMQTTClient.configureEndpoint(host, port)
    myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

#===================================================================
# AWSIoTMQTTClient Last Will and Testament (LWT) configuration
#===================================================================

# Configure LWT entry
payload = {
    "state": {
        "reported": {
            "connected":"false"
        }
    }
}
payloadJSON = json.dumps(payload)
myAWSIoTMQTTClient.configureLastWill(
        "lwt/" + thingId,
        payloadJSON,
        0
    )



# Connect to AWS IoT
myAWSIoTMQTTClient.connect(20)
print('MQTT client connected + Configured LWT: %s: %s\n' % ("lwt/" + thingId, payloadJSON))

#===================================================================
# Send update to device shadow through topic (old version)
#===================================================================

# payload = {
#     "state": {
#         "reported": {
#             "connected":"true",
#             "last connection (UTC)":str(datetime.datetime.now()),
#             "has_log":"2",
#             "has_video":"17",
#             "xml_version": "v2018.10",
#             "sw_version": "v2018.10"
#         }
#     }
# }
# payloadJSON = json.dumps(payload)
# myAWSIoTMQTTClient.publish(
#         "$aws/things/" + thingId + "/shadow/update",
#         payloadJSON,
#         0
#     )
# print('Published topic %s: %s\n' % ("$aws/things/" + thingId + "/shadow/update", payloadJSON))


#===================================================================
# Connect to Shadow and send Payload
#===================================================================

# Shadow client/handler.callbacks setup
myShadowClient = AWSIoTMQTTShadowClient(clientIdShadow)
myShadowClient.configureEndpoint(host, port)
myShadowClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)
myShadowClient.configureConnectDisconnectTimeout(10)  # 10 sec
myShadowClient.configureMQTTOperationTimeout(5)  # 5 sec

myShadowClient.connect()

myShadowHandler = myShadowClient.createShadowHandlerWithName(thingId, True)
myShadowCallbackContainer = shadowCallbackContainer(myShadowHandler)


# Payload
payload = {
    "state": {
        "reported": {
            "connected":"true",
            "last connection (UTC)":str(datetime.datetime.now()),
            "has_log":random.randint(1,31),
            "has_video":random.randint(10,200),
            "xml_version": "v2018.10",
            "sw_version": "v2018.10",
            "shadowtest": "Hallo"
        }
    }
}

payloadJSON = json.dumps(payload)

# Update device Shadow
myShadowHandler.shadowRegisterDeltaCallback(myShadowCallbackContainer.customShadowCallback_DeltaListen)
myShadowHandler.shadowUpdate(payloadJSON,myShadowCallbackContainer.customShadowCallback_Update,5)

# Print shadow:
myShadowHandler.shadowGet(myShadowCallbackContainer.customShadowCallback_Get,5)

print(myShadowCallbackContainer._shadow)
time.sleep(5)
print(myShadowCallbackContainer._shadow)



#=======================================================================
# Broadcast/Listen to topics
#=======================================================================

# Subscribe to AWS IOT
if args.mode == 'both' or args.mode == 'subscribe':
    myAWSIoTMQTTClient.subscribe(topic, 1, customCallback)
time.sleep(2)

# Publish to the same topic in a loop forever
loopCount = 0
while True:
    if args.mode == 'both' or args.mode == 'publish':
        message = {}
        message['message'] = args.message
        message['sequence'] = loopCount
        messageJson = json.dumps(message)
        myAWSIoTMQTTClient.publish(topic, messageJson, 1)
        if args.mode == 'publish':
            print('Published topic %s: %s\n' % (topic, messageJson))
        loopCount += 1
    time.sleep(5)
