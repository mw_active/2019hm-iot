#!/bin/bash
##############################################################
# Sends a live-sign to AWS-IoT
# * declares LWT messgare
#
# Hardcoded values: AWS endpoint
# Certificate locations
###############################################################
export BOLD='\e[1;31m'         # Bold Red
export REV='\e[1;32m'          # Bold Green
export OFF=$(tput sgr0)

echo -e "${REV}==========================================${OFF}"
echo -e "${REV}= Send life signs to AWS-IoT             =${OFF}"
echo -e "${REV}= * Send some random diagnostics         =${OFF}"
echo -e "${REV}= * Register LWT                         =${OFF}"
echo -e "${REV}==========================================${OFF}"

## Find script path
path_tmp=$(readlink -f "${BASH_SOURCE[0]}")
PATH_ACTUAL=$(dirname "$path_tmp")

${PATH_ACTUAL}/livesigns.py -e a3g1ym8g0kwvjd-ats.iot.us-west-2.amazonaws.com -r ${PATH_ACTUAL}/../demo/AmazonRootCA1.pem -c ${PATH_ACTUAL}/../demo/*-certificate.pem.crt -k ${PATH_ACTUAL}/../demo/*-private.pem.key

