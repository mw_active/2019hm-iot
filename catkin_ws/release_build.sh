#!/bin/bash
##############################################################
# Builds the TMLS-ROS package on
# * X86_64 architectures
# * arm architectures (TX2)
#
# Contains the hotfixes for builde dependencies
###############################################################

## Check if environmnet is present
if [ -z "${tmls_DIR+x}" ]; then
    echo -e "${REV}tmls environment not defined, sourcing environment script${OFF}"\\n\\n
    # Define global variables
    path_tmp=$(readlink -f "${BASH_SOURCE[0]}")
    PATH_ACTUAL=$(dirname "$path_tmp")
    . "${PATH_ACTUAL}/../tmls-env.sh"
else
    echo "Environment set to $tmls_DIR"
fi

echo -e "${REV}==========================================${OFF}"
echo -e "${REV}= Build TMLS-ROS                            =${OFF}"
echo -e "${REV}==========================================${OFF}"

usage()
{
    echo "usage: release_build.sh [[-c] [-h]]"
}


## Main

targetpartition=/dev/sdb

while [ "$1" != "" ]; do
    case $1 in
        -c | --clean )          rm -rf build devel
                                echo -e "${REV}= - Removed build and devel directories  =${OFF}"
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ "$tmls_ARCH" = "aarch64" ]; then
  echo -e "${REV}= - Build on ${tmls_ARCH}                      =${OFF}"
  catkin_make -DCATKIN_ENABLE_TESTING=True \
              -DCMAKE_BUILD_TYPE=Release \
              -DCATKIN_BLACKLIST_PACKAGES="realsense2_camera" \
              --pkg piksi_rtk_msgs
  catkin_make -DCATKIN_ENABLE_TESTING=True \
              -DCMAKE_BUILD_TYPE=Release \
              -DCATKIN_BLACKLIST_PACKAGES="realsense2_camera"
fi

if [ "$tmls_ARCH" = "x86_64" ]; then
  echo -e "${REV}= - Build on ${tmls_ARCH}                     =${OFF}"
  catkin_make -DCATKIN_ENABLE_TESTING=True \
              -DCMAKE_BUILD_TYPE=Release \
              -DCATKIN_BLACKLIST_PACKAGES="camera_trigger"
fi


## Source environment again
. "${tmls_DIR}/tmls-env.sh"
