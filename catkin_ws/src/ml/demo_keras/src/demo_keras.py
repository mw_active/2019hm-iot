#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Simple object detection with Keras in ROS

This module demonstrates how to use machine learning in combination with the Robot Operating System ROS.
It is written to be part of the ROS build system catkin.

Most of this code is based on Matthew's `Visual Object Recognition Blogpost`_.

.. _Visual Object Recognition Blogpost: http://projectsfromtech.blogspot.com/2017/10/visual-object-recognition-in-ros-using.html
"""

# Python 3 compatibility
from __future__ import print_function

# Import Scientific and Vision Libraries
import cv2  # OpenCV
import scipy as sp # I prefer scipy instead of numpy
import pylab as plt
import sys
import platform
import json
import os

# Impport ROS base packages
import rospy
import roslib

# Import the messages for topic broadcasts
from std_msgs.msg import String
from std_msgs.msg import Float32
from sensor_msgs.msg import Image
from aws_mqtt_bridge.msg import MQTT_publish

# Import OpenCV image handling within ROS.
from cv_bridge import CvBridge, CvBridgeError

# Import Keras and Tensorflow
import tensorflow as tf
from keras.preprocessing import image
from keras.applications.resnet50 import ResNet50, preprocess_input, decode_predictions


class DemoKeras(object):
  """ Class to demonstrate ML on ROS with Keras and OpenCV
  """
  def __init__(self, topic="test/keras_object"):
    """ Initializes the class to be ready for ROS

    Args:
      topic (str): MQTT topic to publsih to.

    """
    print("="*50)
    print("Initializing intsance of DemoKeras class")
    print("="*50)
    self._srcdir = os.path.dirname(os.path.abspath(__file__))
    print("Code source directory: " + self._srcdir)
    print("OpenCV version: " + cv2.__version__)
    self._target_size = (224, 224) # Rescale image size to this value
    # Prepare Messages
    self._msg_string = String()
    self._msg_float  = Float32()
    self._msg_mqtt = MQTT_publish()
    self._mqtt_shadow = {
      "state":{
        "reported":{
          "ros_status":"ROS is running",
          "keras_object":"test",
          "keras_prop":"test"
        }
      }
    }
    self._mqtt_topic = topic

    # Prepare Keras Model
    self._model, self._graph = self._initialize_keras()
    # Prepare ROS publishers
    self._pub, self._bridge = self._initialize_ros()
    # Prepare OpenCV Haar cascade:
    self._cv2cascade = self._initialize_cv()

  def _initialize_keras(self):
    """ Initialize Keras

    Returns:
      model, graph: The ML model and tensorflow compute graph.
    """
    model = ResNet50(weights='imagenet')
    model._make_predict_function()
    graph = tf.get_default_graph()

    return model, graph

  def _initialize_cv(self):
    """ Initialize simple face recognition based on a Haar cascade

    Returns:
      cascade: the cvs cascade object
    """
    cascade = {}

    fname = os.path.join(self._srcdir,'haarcascade_frontalface_default.xml')
    cascade["face"] = cv2.CascadeClassifier(fname)

    fname = os.path.join(self._srcdir,'haarcascade_eye.xml')
    cascade["eyes"] = cv2.CascadeClassifier(fname)

    return cascade



  def _initialize_ros(self):
    """Initialize the ROS node

    Returns:
      pub(dict), bridge: Dictionary of publishers and OpenCV bridge
    """
    rospy.init_node('demo_keras', anonymous=True)
    # Prepare message publisher
    pub = {}
    pub["object"] = rospy.Publisher('object_detected', String, queue_size = 1)
    pub["propability"] = rospy.Publisher('object_detected_probability', Float32, queue_size = 1)
    pub["mqtt"] = rospy.Publisher('object_detected_mqtt',MQTT_publish, queue_size = 1 )
    pub["image"] = rospy.Publisher('keras_image',Image, queue_size = 1)
    pub["face"] = rospy.Publisher('cv2_face',Image, queue_size = 1)
    # Create OpenCV for image resizing
    bridge = CvBridge()

    return pub, bridge

  def preprocess_image(self,image_msg):
    """Convert image message

    """
    # Use the opencv bridge to convert the ros image messsage into OpenCV object.
    image = self._bridge.imgmsg_to_cv2(image_msg, desired_encoding="passthrough")
    # Create a square image
    height, width, channels = image.shape
    x = height if height > width else width
    y = height if height > width else width
    image = image[0:y,0:x]
    # Resize image
    image = cv2.resize(image, self._target_size)
    # Broadcast resized image into topic
    self._pub['image'].publish(
        self._bridge.cv2_to_imgmsg(image,encoding='rgb8')
    )
    # Recast image to an array
    image = sp.asarray(image)
    # Make it tensorflow compatible
    image = sp.expand_dims(image, axis=0)
    # Convert to float64
    image = image.astype(float)
    # Apply data regularization
    image = preprocess_input(image)

    return image

  def cv2_detect(self,image_msg):
    """ Detect faces and eyes in an image message:

    Source: OpenCV Manual (https://docs.opencv.org/3.3.1/d7/d8b/tutorial_py_face_detection.html)

    Args:
      img_msg: ROS Image message

    Returns:
      img (ndarray): OpenCV image object
    """
    ## Preprocess image message:
    # TODO: Combine this with the preprocess function
    # Use the opencv bridge to convert the ros image messsage into OpenCV object.
    image = self._bridge.imgmsg_to_cv2(image_msg, desired_encoding="passthrough")
    # Create a square image
    height, width, channels = image.shape
    x = height if height > width else width
    y = height if height > width else width
    image = image[0:y,0:x]
    # Resize image
    image = cv2.resize(image, self._target_size)
    # Convert to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    ## Apply Haar cascade
    faces = self._cv2cascade['face'].detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE # Deprecated call: cv2.cv.CV_HAAR_SCALE_IMAGE
    )

    # Draw a rectangle around the faces and detect eyes
    for (x, y, w, h) in faces:
        cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = image[y:y+h, x:x+w]
        eyes = self._cv2cascade['eyes'].detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(255,0,0),1)

    # Broadcast resized image into topic
    self._pub['face'].publish(
        self._bridge.cv2_to_imgmsg(image,encoding='rgb8')
    )

    return image


  def ros_callback(self,image_msg):
    """ Ros subscriber callback function

    """
    # Convert image message into numpy array
    sp_image = self.preprocess_image(image_msg)

    # Start ML processing pipeline
    with self._graph.as_default():
      # Classify the image
       pred = self._model.predict(sp_image)
       # decode returns a list  of tuples [(class,description,probability),...
       pred_string = decode_predictions(pred, top=1)[0]   # Decode top 1 predictions
       self._msg_string.data = pred_string[0][1]
       self._msg_float.data = float(pred_string[0][2])
       self._pub["object"].publish(self._msg_string)
       self._pub["propability"].publish(self._msg_float)

       self._msg_mqtt.topic = self._mqtt_topic
       self._mqtt_shadow["state"]["reported"]["keras_object"]=str(pred_string[0][1])
       self._mqtt_shadow["state"]["reported"]["keras_prob"]=str(pred_string[0][2])
       self._msg_mqtt.payload = json.dumps(self._mqtt_shadow)
       self._pub["mqtt"].publish(self._msg_mqtt)

       self.cv2_detect(image_msg)



def main(argv):
  '''Main function.

  This function creates a classifier object, subscribes to the camera
  topic and the spins until ros is terminated.
  '''

  topic = '$aws/things/' + platform.node() + '/shadow/update'
  myClassifier = DemoKeras(topic=topic)
  rospy.Subscriber("usb_cam/image_raw", Image,
                    myClassifier.ros_callback,
                    queue_size = 1, buff_size = 16777216
  )

  while not rospy.is_shutdown():
    rospy.spin()


if __name__ == '__main__':
  main(sys.argv)




