#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Simple object detection with Keras in ROS

This module demonstrates how to use machine learning in combination with the Robot Operating System ROS.
It is written to be part of the ROS build system catkin.

Most of this code is based on `Visual Object Recognition Blogpost`_.

.. _Visual Object Recognition Blogpost: http://projectsfromtech.blogspot.com/2017/10/visual-object-recognition-in-ros-using.html
"""

# Import Scientific and Vision Libraries
import cv2  # OpenCV
import scipy as sp # I prefer scipy instead of numpy

# Impport ROS base packages
import rospy
import roslib
import json

# Import the messages for topic broadcasts
from std_msgs.msg import String
from std_msgs.msg import Float32
from sensor_msgs.msg import Image
from aws_mqtt_bridge.msg import MQTT_publish

# Import OpenCV image handling within ROS.
from cv_bridge import CvBridge, CvBridgeError

# Import Keras and Tensorflow
import tensorflow as tf
from keras.preprocessing import image
from keras.applications.resnet50 import ResNet50, preprocess_input, decode_predictions


# import model and  implement fix found here.
# https://github.com/fchollet/keras/issues/2397
model = ResNet50(weights='imagenet')
model._make_predict_function()
graph = tf.get_default_graph()
target_size = (224, 224)


rospy.init_node('classify', anonymous=True)
#These should be combined into a single message
pub = rospy.Publisher('object_detected', String, queue_size = 1)
pub1 = rospy.Publisher('object_detected_probability', Float32, queue_size = 1)
pub2 = rospy.Publisher('object_detected_mqtt',MQTT_publish, queue_size = 1 )
bridge = CvBridge()

msg_string = String()
msg_float = Float32()
msg_mqtt = MQTT_publish()
mqtt_shadow = {
  "state":{
    "reported":{
      "ros_status":"ROS is running",
      "keras_object":"test",
      "keras_prop":"test"
    }
  }
}


def callback(image_msg):
    #First convert the image to OpenCV image
    cv_image = bridge.imgmsg_to_cv2(image_msg, desired_encoding="passthrough")
    cv_image = cv2.resize(cv_image, target_size)  # resize image
    sp_image = sp.asarray(cv_image)               # read as sp array
    sp_image = sp.expand_dims(sp_image, axis=0)   # Add another dimension for tensorflow
    sp_image = sp_image.astype(float)  # preprocess needs float64 and img is uint8
    sp_image = preprocess_input(sp_image)         # Regularize the data

    global graph                                  # This is a workaround for asynchronous execution
    with graph.as_default():
       preds = model.predict(sp_image)            # Classify the image
       # decode returns a list  of tuples [(class,description,probability),(class, descrip ...
       pred_string = decode_predictions(preds, top=1)[0]   # Decode top 1 predictions
       msg_string.data = pred_string[0][1]
       msg_float.data = float(pred_string[0][2])
       pub.publish(msg_string)
       pub1.publish(msg_float)

       msg_mqtt.topic = "$aws/ttttt"
       mqtt_shadow["state"]["reported"]["keras_object"]=str(pred_string[0][1])
       mqtt_shadow["state"]["reported"]["keras_prov"]=str(pred_string[0][2])
       msg_mqtt.payload = json.dumps(mqtt_shadow)
       #print(mqtt_shadow)
       #msg_mqtt.payload = "Hallo"
       pub2.publish(msg_mqtt)

rospy.Subscriber("cv_camera/image_raw", Image, callback, queue_size = 1, buff_size = 16777216)



while not rospy.is_shutdown():
  rospy.spin()
